<link href="{{asset('css/navbar.css')}}" rel="stylesheet" media="screen">

<div class="wrapper">

    <!-- Sidebar  -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3 class="test_header"><a href="{{url('homepage')}}">E-Document</a></h3>
        </div>

        <ul class="list-unstyled CTAs button_create">
            <li>
                <a href="{{url('create')}}" class="download">Create Document</a>
            </li>
        </ul>
        <ul class="list-unstyled components">
            {{--<p>Dummy Heading</p>--}}
            <li>
                <a href="#">All Document</a>
            </li>
            <li>
                <a href="#">Portfolio</a>
            </li>
            <li>
                <a href="#">Contact</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <a href="#" class="download">All Member</a>
            </li>
            <li>
                <a href="#" class="article">Log out</a>
            </li>
        </ul>
    </nav>

</div>



