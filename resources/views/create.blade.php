@extends('layout.app')
@section('content')
    <script src="https://unpkg.com/vue-recaptcha@latest/dist/vue-recaptcha.js"></script>
    <!-- Page Content  -->
    <div id="content">
        <nav class="navbar navbar-expand-lg navbar-light bg-light search-bar">
            <div class="col-12 col-md-10 col-lg-10">
                <form class="card card-sm">
                    <div class="card-body row no-gutters align-items-center">
                        <div class="col-auto">
                            <i class="fas fa-search h4 text-body"></i>
                        </div>
                        <!--end of col-->
                        <div class="col">
                            <input class="form-control form-control-lg form-control-borderless" type="search"
                                   placeholder="Search topics or keywords">
                        </div>
                        <!--end of col-->

                    </div>
                </form>
            </div>
        </nav>
        <div class="container" style="margin-top: 64px;">
            <h2>Test vue.js</h2>
            <div id="app">
                <create-document></create-document>
            </div>
        </div>
    </div>

@endsection